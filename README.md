# Android-Codelabs
## All practicing from https://codelabs.developers.google.com

### Section 1: [Android Kotlin Fundamentals](https://developer.android.com/courses/kotlin-android-fundamentals/overview)
* [Lesson 1: Build your first app](https://codelabs.developers.google.com/codelabs/kotlin-android-training-install-studio/index.html)
* [Lesson 2: Layouts]()
* [Lesson 3: Navigation]()
* [Lesson 4: Activity and fragment lifecycles]()
* [Lesson 5: Architecture components]()
* [Lesson 6: Room database and coroutines]()
* [Lesson 7: RecyclerView]()
* [Lesson 8: Connecting to the internet]()
* [Lesson 9: Repository]()
* [Lesson 10: Designing for everyone]()

### Section 2: [Advanced Android in Kotlin](https://developer.android.com/courses/kotlin-android-advanced/overview)
* [Lesson 1: Notifications]()
* [Lesson 2: Advanced Graphics]()
* [Lesson 3: Animation]()
* [Lesson 4: Geo]()
* [Lesson 5: Testing and Dependency Injection]()
* [Lesson 6: Login]()

